	.file	"coseno.c"
	.intel_syntax noprefix
	.text
	.globl	main
	.type	main, @function
main:
.LFB0:
	.cfi_startproc
	push	rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	mov	rbp, rsp
	.cfi_def_cfa_register 6
	mov	eax, DWORD PTR .LC0[rip]
	mov	DWORD PTR [rbp-24], eax
	mov	eax, DWORD PTR .LC1[rip]
	mov	DWORD PTR [rbp-28], eax
	movss	xmm0, DWORD PTR [rbp-24]
	mulss	xmm0, DWORD PTR [rbp-24]
	movss	DWORD PTR [rbp-32], xmm0
	mov	DWORD PTR [rbp-12], 0
	mov	DWORD PTR [rbp-16], -1
	mov	eax, DWORD PTR .LC2[rip]
	mov	DWORD PTR [rbp-8], eax
	mov	eax, DWORD PTR [rbp-8]
	mov	DWORD PTR [rbp-4], eax
	jmp	.L2
.L5:
	movss	xmm0, DWORD PTR [rbp-8]
	mulss	xmm0, DWORD PTR [rbp-32]
	movss	DWORD PTR [rbp-8], xmm0
	mov	DWORD PTR [rbp-20], 0
	jmp	.L3
.L4:
	add	DWORD PTR [rbp-12], 1
	cvtsi2ss	xmm0, DWORD PTR [rbp-12]
	movss	xmm1, DWORD PTR [rbp-8]
	movaps	xmm2, xmm1
	divss	xmm2, xmm0
	movaps	xmm0, xmm2
	movss	DWORD PTR [rbp-8], xmm0
	add	DWORD PTR [rbp-20], 1
.L3:
	cmp	DWORD PTR [rbp-20], 1
	jle	.L4
	cvtsi2ss	xmm0, DWORD PTR [rbp-16]
	mulss	xmm0, DWORD PTR [rbp-8]
	movss	xmm1, DWORD PTR [rbp-4]
	addss	xmm0, xmm1
	movss	DWORD PTR [rbp-4], xmm0
	neg	DWORD PTR [rbp-16]
.L2:
	movss	xmm0, DWORD PTR [rbp-8]
	ucomiss	xmm0, DWORD PTR [rbp-28]
	ja	.L5
	mov	eax, 0
	pop	rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	main, .-main
	.section	.rodata
	.align 4
.LC0:
	.long	1069966950
	.align 4
.LC1:
	.long	897988541
	.align 4
.LC2:
	.long	1065353216
	.ident	"GCC: (Debian 4.7.2-5) 4.7.2"
	.section	.note.GNU-stack,"",@progbits
