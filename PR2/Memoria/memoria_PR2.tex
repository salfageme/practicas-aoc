\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[spanish]{babel}
\usepackage{listings}
\usepackage{xcolor}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[
top=2cm,
bottom=3cm]{geometry}
\usepackage{sectsty} % Allows customizing section commands
\allsectionsfont{\centering \normalfont\scshape}
\usepackage{fancyhdr} % Custom headers and footers
\pagestyle{fancyplain} % Makes all pages in the document conform to the custom headers and footers
\fancyhead{} % No page header - if you want one, create it in the same way as the footers below
\fancyfoot[L]{\scriptsize{\textbf{PR2-AOC}}}
\fancyfoot[C]{\footnotesize{{\scshape Samuel Alfageme} (\url{samuel.alfageme@alumnos.uva.es})}} 
\fancyfoot[R]{\thepage} % Page numbering for right footer
\renewcommand{\headrulewidth}{0pt} % Remove header underlines
\renewcommand{\footrulewidth}{0pt} % Remove footer underlines
\renewcommand{\lstlistingname}{Algoritmo}
\setlength{\headheight}{13.6pt} % Customize the height of the header
\lstdefinestyle{customc}{
  belowcaptionskip=1\baselineskip,
  breaklines=true,
  frame=single,
  xleftmargin=\parindent,
  xrightmargin=\parindent,
  language=C,
  showstringspaces=false,
  basicstyle=\footnotesize\ttfamily,
  keywordstyle=\bfseries\color{green!40!black},
  commentstyle=\color{purple!40!black},
  identifierstyle=\color{blue},
  stringstyle=\color{orange},
}

\lstdefinestyle{customasm}{
  belowcaptionskip=1\baselineskip,
  frame=single,
  xleftmargin=\parindent,
  xrightmargin=\parindent,
  language=[x86masm]Assembler,
  morekeywords={faddp,e},
  numbers=left,
  numberstyle=\tiny\color{gray},
  basicstyle=\footnotesize\ttfamily,
  commentstyle=\color{gray},
}
\lstdefinestyle{custombash}{
  backgroundcolor=\color{white},
  belowcaptionskip=1\baselineskip,
  frame=single,
  %xleftmargin=\parindent,
  language=bash,
  morekeywords={nasm,ld,gcc},
  basicstyle=\footnotesize\ttfamily,
  commentstyle=\color{purple!40!black},
}
\fancypagestyle{firststyle}
{
   \fancyhf{}
   
}
\lstset{escapechar=@,style=customc}
\title{\includegraphics[height=3.5cm]{UVaLogo.jpg} \\ {\scshape Práctica 2} \\ \normalsize{\scshape Arquitectura y Organización de Computadores}\\[14.0cm]}
\author{\scshape Samuel Alfageme Sainz \\ \small{\scshape L1 - 2ºGºIng.Informática} \\ \footnotesize{\scshape Universidad De Valladolid}}
\date{\vspace{-5ex}}
\begin{document}


\maketitle
\thispagestyle{firststyle}
\newpage
\section{Introducción}
\noindent
En esta práctica se ha profundizado en el uso del lenguaje ensamblador para {\ttfamily IA32}, centrándonos en 2 aspectos concretos del mismo como son:
\begin{itemize}
\item{El manejo del conjunto de instrucciones del repertorio x87 (FP).}
\item{La inserción de una función escrita en ensamblador en código de alto nivel.}
\end{itemize}
Para ello, se pide que se realice el cálculo de la función $y$, descrita a continuación, a partir del repertorio de instrucciones de punto flotante x87. 
\begin{equation}
y = 10^{x}\cdot\frac{\sqrt{1+x^{2}}}{sin(x)}
\end{equation}
Para realizar cálculos complejos como el de $10^{x}$, para los que no disponemos de instrucciones concretas, recurriremos a las propiedades matemáticas de los logaritmos; podemos realizar la transformación $10^{x}=2^{x\cdot\log_{2}10}$ dado que tanto el cálculo de potencias en base 2\footnote{Hay que tener en cuenta que el cálculo de $2^{x}$ con $x\in\mathbb{R}$ tampoco puede ser realizado directamente: es necesario separar la parte entera y la parte decimal del exponente para calcular el valor de la potencia de cada uno (mediante las instrucciones {\ttfamily f2xm1} y {\ttfamily fscale} respectivamente) y multiplicar ambos para obtener el valor final de $2^{x}$} como la constante $log_{2}10$ sí que se encuentran en el conjunto de instrucciones del coprocesador de punto flotante.
\\[0.5cm]
La práctica se estructura en 2 archivos de código fuente\footnote{Todos los archivos fuente {\ttfamily .asm} pueden ser descargados de mi directorio personal pulsando sobre el enlace correspondiente a su archivo en la sección 2: \emph{Código Fuente}} el primero de ellos, escrito en {\ttfamily C} realiza la petición del valor de la variable $x$ al usuario y la llamada a la función {\ttfamily calculo()} cuya implementación se describe en el Archivo 2 que calcula y almacena el valor asociado a la función $y$ para el valor concreto de $x$ en la variable de punto flotante {\ttfamily y}. 
Para no aumentar la complejidad del código, la comunicación (paso de argumentos y retorno de resultados) entre la función y el programa principal se realiza mediante variables globales. Una vez realizado el cálculo, el programa imprimirá su resultado y terminará su ejecución.

\section{Código Fuente}
\renewcommand{\lstlistingname}{Archivo}
\setcounter{lstlisting}{0}
\lstinputlisting[caption=\href{http://alumnos.inf.uva.es/samalfa/AOC/PR2.c}{\ttfamily \underline{PR2.c}}, style=customc]{PR2.c}
\newpage
\lstinputlisting[caption=\href{http://alumnos.inf.uva.es/samalfa/AOC/calculo.asm}{\ttfamily \underline{calculo.asm}}, style=customasm]{calculo.asm}
Cabe señalar lo que ocurre en las líneas {\ttfamily 38} a {\ttfamily 45}: 
\lstinputlisting[style=customasm, firstline=38, lastline=45, firstnumber=38]{calculo.asm}
Cómo se ha explicado en el pie de página de la Introducción, necesitamos extraer la parte entera del exponente para emplear la instrucción {\ttfamily fscale}; esta operación se realiza mediante la instrucción {\ttfamily frndint}, la cual, con el valor por defecto del registro de control de la FPU redondea al entero más próximo el exponente, cuando nuestro objetivo real es extraer la parte entera del mismo (truncar), por este motivo, en las líneas anteriormente citadas realizamos un cambio a {\ttfamily 11} de los 2 bits de Rounding Control del registro de control del coprocesador, consiguiendo así el efecto deseado.
\appendix
\renewcommand{\lstlistingname}{Fig.}
\setcounter{lstlisting}{0}
\section{Ejemplo de ejecución del programa}
\lstinputlisting[caption=Compilación y enlazado, firstline=1, lastline=2, style=custombash]{ejec.sh}
\lstinputlisting[caption=Ejecución, firstline=3, style=custombash]{ejec.sh}
\end{document}
